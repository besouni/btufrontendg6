function show_cookies(){
    console.log(document.cookie);
    console.log(sessionStorage.getItem("user"));
}

function create_simple_cookie(){
    document.cookie = "user=btu";
}

create_simple_cookie();

function create_cookie(){
    var key = document.getElementById("key").value;
    var value = document.querySelector("#value").value;
    var seconds = document.getElementById("seconds").value;
    var d = new Date();
    console.log(d);
    console.log(d.getTime());
    d.setTime(d.getTime()+seconds*1000);
    console.log(d);
    document.cookie = key+"="+value + "; expires=" + d + "; /btufrontendg6/Lecture_13";
}

function delete_cookie() {
    var key = document.getElementById("key").value;
    document.cookie = key + "=; Max-Age=0";
}

function create_storage(){
    sessionStorage.setItem("user", "beso01");
    sessionStorage.setItem("user2", "beso03");
}

function delete_storage(){
    sessionStorage.removeItem("user");
}