async function my_click(){
    let promise = new Promise(
        (resolve, reject) =>
        {
            var request = new XMLHttpRequest();
            request.open("POST", "../request/gallery.html")
            request.send();
            request.onload = function (){
                // console.log(request.status);
                if(request.status == 200){
                    resolve(request.responseText);
                }else{
                    reject(request.status);
                }
            }
        });

    // promise.then(
    //     function (value){
    //         console.log("Hello "+value)
    //     }
    // )

    let result = await promise; // wait until the promise resolves (*)

    show_result(result) // "done!"
}


// async function f() {
//     let x = 87;
//     console.log("F is running!!!!")
//     return x-80;
// }
//
//
//
// function my_click(){
//     f().then((value)=>{
//         console.log(value)
//     })
//     // console.log(f())
// }


// function my_click(){
//     new Promise(function(resolve, reject) {
//
//         setTimeout(
//             function() { resolve(1) },
//             // () => {resolve(1)}
//             1000); // (*)
//
//     }).then(function(param) { // (**)
//
//         // alert(param); // 1
//         console.log(param)
//         return param * 2;
//
//     }).then(function(result) { // (***)
//
//         // alert(result); // 2
//         console.log(result)
//         return result + 12;
//
//     }).then(function(result) {
//
//         // alert(result); // 4
//         console.log(result)
//         return result - 10;
//     }).then((result)=>{
//         console.log(result)
//         // alert(result)
//     })
// }


// let myPromise = new Promise(function(myResolve, myReject) {
// // "Producing Code" (May take some time)
//     let x = 70;
//     let y = 34;
//     // myResolve(x+y);
//
//     if(x < y){
//         myResolve(x*y)
//     }else{
//         myReject(x - y);
//     }
// });

// "Consuming Code" (Must wait for a fulfilled Promise)

// function my_click(){
//     myPromise.then(
//         function(value) {
//             show_result(value)
//         },
//
//     );
// }



// function f1(){
//     console.log(12);
// }
//
// function my_click(){
//     // console.log("===")
//     // setTimeout(f1, 2000)
//     sum(3, 4, show_result)
// }
//
// function sum(n1, n2, callBack){
//     callBack(n1+n2);
// }
//
function show_result(sum){
    document.getElementById("demo").innerHTML = sum
}
